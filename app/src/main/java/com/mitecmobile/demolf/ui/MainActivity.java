package com.mitecmobile.demolf.ui;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mitecmobile.demolf.R;
import com.mitecmobile.demolf.controller.media.MediaPlayerController;
import com.mitecmobile.demolf.controller.scanner.IScannerListener;
import com.mitecmobile.demolf.controller.scanner.ScannerController;
import com.rscja.deviceapi.exception.ConfigurationException;

public class MainActivity extends AppCompatActivity implements IScannerListener {

    private AlertDialog progressDialog;
    private TextView message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        message = findViewById(R.id.message);
        try {
            ScannerController.getInstance().initScanner(this);
            showProgressDialog(R.string.common_init);
        } catch (ConfigurationException e) {
            Toast.makeText(this, "Error al iniciar el escaner. Por favor, revise que está bien conectado y que no está siendo utilizado por otra app.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (KeyEvent.KEYCODE_F9 == keyCode) {
            ScannerController.getInstance().scanItem();
            showProgressDialog(R.string.common_scan);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showProgressDialog(@StringRes int message) {
        View view = getLayoutInflater().inflate(R.layout.dialog_progress, null);
        TextView textView = view.findViewById(R.id.progress_message);
        textView.setText(message);
        progressDialog = new AlertDialog.Builder(this)
                .setView(view)
                .setCancelable(false)
                .create();
        progressDialog.show();
    }

    private void hideProgressDialog() {
        progressDialog.dismiss();
        progressDialog = null;
    }

    @Override
    protected void onDestroy() {
        ScannerController.getInstance().stopScanner();
        super.onDestroy();
    }

    @Override
    public void initResult(boolean result) {
        hideProgressDialog();
    }

    @Override
    public void showScanResult(String scanResult) {
        hideProgressDialog();
        int resId = scanResult == null ? R.raw.ko : R.raw.ok;
        MediaPlayerController.getInstance().play(this, resId);
        if (scanResult == null)
            scanResult = "No hay resultados";
        else if(scanResult.length()>=6){
            scanResult = scanResult + "\n\n" + scanResult.substring(scanResult.length() - 6);
        }
        message.setText(scanResult);
    }
}
