package com.mitecmobile.demolf.controller.scanner;

import android.os.AsyncTask;

import com.rscja.deviceapi.RFIDWithLF;
import com.rscja.deviceapi.exception.ConfigurationException;

/**
 * Clase que gestiona las conexiones con el escaner
 */
public class ScannerController {

    private static ScannerController instance = new ScannerController();
    private RFIDWithLF scanner;
    private IScannerListener listener;

    private ScannerController() {
        //singleton
    }

    public static ScannerController getInstance() {
        return instance;
    }

    /**
     * Inicializa el escaner
     *
     * @throws ConfigurationException si el hardware no está disponible
     */
    public void initScanner(IScannerListener listener) throws ConfigurationException {
        this.listener = listener;
        scanner = RFIDWithLF.getInstance();
        new InitTask().execute();
    }

    public void scanItem() {
        new SearchTask().execute();
    }

    /**
     * Libera el escaner para que esté disponible en otras apps.
     */
    public boolean stopScanner() {
        return scanner.free();
    }

    /**
     * Task de inicialización del escaner, necesario realizarse en segundo plano
     */
    private class InitTask extends AsyncTask<String, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            return scanner.init();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            listener.initResult(result);
        }
    }

    /**
     * Realiza una búsqueda de tags LF, necesario realizarse en segundo plano
     */
    private class SearchTask extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            return scanner.readDataWithIDCard(0);
        }

        @Override
        protected void onPostExecute(String result) {
            listener.showScanResult(result);
        }
    }
}
