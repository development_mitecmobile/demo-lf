package com.mitecmobile.demolf.controller.scanner;

public interface IScannerListener {

    void initResult(boolean result);

    void showScanResult(String scanResult);
}
