package com.mitecmobile.demolf.controller.media;

import android.content.Context;
import android.media.MediaPlayer;


public class MediaPlayerController {

    private static MediaPlayerController instance;
    private MediaPlayer mediaPlayer = null;

    private MediaPlayerController() {
    }

    public static MediaPlayerController getInstance() {
        if (instance == null)
            instance = new MediaPlayerController();
        return instance;
    }

    public void play(Context contxt, int resId) {
        if (contxt == null) {
            return;
        }

        if (resId == 0) {
            return;
        }

        release();
        mediaPlayer = MediaPlayer.create(contxt, resId);
        mediaPlayer.start();
    }

    public void release() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

}
